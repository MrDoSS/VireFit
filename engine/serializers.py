from rest_framework.serializers import ModelSerializer

from engine.models import Device


class DeviceSerializer(ModelSerializer):
    class Meta:
        model = Device
        fields = ('unique_id',)