from django.apps import AppConfig


class TrainingsConfig(AppConfig):
    name = 'engine.trainings'
    verbose_name = 'Тренировки'