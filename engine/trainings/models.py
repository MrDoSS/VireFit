from django.db import models
from django.utils.translation import ugettext as _


class TrainingType(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Название'))
    slug = models.SlugField(primary_key=True, verbose_name=_('Ярлык'))

    class Meta:
        verbose_name = _('вид тренировки')
        verbose_name_plural = _('виды тренировок')

    def __str__(self):
        return self.name
