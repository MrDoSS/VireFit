from django.core.exceptions import ObjectDoesNotExist
from rest_framework.generics import CreateAPIView
from django.utils.translation import ugettext as _
from rest_framework.response import Response
from engine.models import Device, License


class CreateDevice(CreateAPIView):
    def post(self, request, *args, **kwargs):
        response_data = {}
        try:
            lic = License.objects.get(
                activate_code=request.data['activate_code'])  # Получаем лицензию по ключу
            if lic.activation_counter < lic.devices_counter:
                device = Device.objects.create(license=lic)
                response_data['status'] = 'success'
                response_data['message'] = 'Устройство успешно активировано'
                response_data['device_id'] = device.unique_id
            else:
                response_data['status'] = 'error'
                response_data['message'] = _('Активировано максимальное количество устройств по данному ключу')
        except ObjectDoesNotExist:
            response_data['status'] = 'error'
            response_data['message'] = _('Ключ активации невалиден')
        return Response(response_data)


