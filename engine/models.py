import uuid
from django.db import models
from django.utils.translation import ugettext as _
from engine.utils import str_from_uuid
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver


class FitnessClub(models.Model):
    name = models.CharField(max_length=255, verbose_name=_('Название'))
    address = models.CharField(max_length=255, verbose_name=_('Адрес'))
    email = models.EmailField(verbose_name=_('E-mail'))
    admin_fio = models.CharField(max_length=255, verbose_name=_('ФИО администратора'))
    phone = models.CharField(max_length=18, verbose_name=_('Номер телефона'))
    other_info = models.TextField(verbose_name=_('Дополнительная информация'), null=True, blank=True)
    secret_word = models.CharField(max_length=255, verbose_name=_('Секретное слово'))

    class Meta:
        verbose_name = _('фитнесс-клуб')
        verbose_name_plural = _('фитнесс-клубы')

    def __str__(self):
        return '%s, %s' % (self.name, self.address)


class License(models.Model):
    fitness_club = models.ForeignKey(FitnessClub,
                                     verbose_name=_('Фитнесс-клуб'), on_delete=models.CASCADE)
    activate_code = models.CharField(max_length=255, default=uuid.uuid4, verbose_name=_('Код активации'), unique=True)
    price = models.PositiveIntegerField(verbose_name=_('Цена'))
    created = models.DateField(auto_now_add=True, editable=False)
    ending = models.DateField(verbose_name=_('Окончание подписки'), null=True)
    devices_counter = models.PositiveSmallIntegerField(verbose_name=_('Количество обородования'))
    activation_counter = models.PositiveSmallIntegerField(verbose_name=_('Количество активаций'),
                                                          default=0,
                                                          )

    class Meta:
        verbose_name = _('лицензия')
        verbose_name_plural = _('лицензии')

    def __str__(self):
        return 'Лицензия для "%s"' % self.fitness_club


class Device(models.Model):
    unique_id = models.CharField(primary_key=True, max_length=255, verbose_name=_('Уникальный идентификатор'),
                                 unique=True,
                                 default=str_from_uuid)
    license = models.ForeignKey('engine.License', verbose_name=_('Лицензия'),
                                on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('оборудование')
        verbose_name_plural = _('оборудование')

    def __str__(self):
        return 'Оборудование #%s' % self.unique_id


@receiver(post_save, sender=Device)
def increment_activation(sender, instance, created, **kwargs):
    if created:
        instance.license.activation_counter += 1
        instance.license.save()


@receiver(post_delete, sender=Device)
def decrement_activation(sender, instance, **kwargs):
    instance.license.activation_counter -= 1
    instance.license.save()
