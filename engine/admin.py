from django.contrib import admin

from engine.models import Device
from engine.metrics.models import ApplicationStartCounter, ApplicationTime, TrainingStartCounter, TrainingTime
from engine.models import FitnessClub, License
from engine.trainings.models import TrainingType

admin.site.register(FitnessClub)
admin.site.register(License)
admin.site.register(ApplicationTime)
admin.site.register(ApplicationStartCounter)
admin.site.register(TrainingStartCounter)
admin.site.register(TrainingTime)
admin.site.register(Device)


@admin.register(TrainingType)
class TrainingTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}