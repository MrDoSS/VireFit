from django.contrib import admin
from django.urls import path, include

from engine.metrics import urls as metrics
from engine.views import CreateDevice

urlpatterns = [
    path('admin/', admin.site.urls),
    path('devices/create/', CreateDevice.as_view(), name='device_create'),
    path('metrics/', include(metrics.router.urls))
]
