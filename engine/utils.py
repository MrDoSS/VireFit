import uuid


def str_from_uuid():
    return str(uuid.uuid4())[:13]
