from django.apps import AppConfig


class MetricsConfig(AppConfig):
    name = 'engine.metrics'
    verbose_name = 'Метрики'
