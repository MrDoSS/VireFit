from rest_framework.routers import DefaultRouter
from engine.metrics import views

router = DefaultRouter()
router.register(r'app-time', views.ApplicationTimeViewSet)
router.register(r'app-start-counter', views.ApplicationStartCounterViewSet)
router.register(r'training-time', views.TrainingTimeViewSet)
router.register(r'training-start-counter', views.TrainingStartCounterViewSet)