from django.db import models
from django.utils.translation import ugettext as _


class BaseMetric(models.Model):
    device = models.ForeignKey('engine.Device', verbose_name=_('Оборудование'), on_delete=models.CASCADE)
    start_date = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.device)


class BaseMetricTime(BaseMetric):
    time_work = models.CharField(max_length=255, verbose_name=_('Время работы'))

    class Meta:
        abstract = True


class BaseMetricCounter(BaseMetric):
    counter = models.PositiveSmallIntegerField(verbose_name=_('Счетчик запусков'))

    class Meta:
        abstract = True


class ApplicationTime(BaseMetricTime):
    class Meta:
        verbose_name = _('время работы приложения')
        verbose_name_plural = _('время работы приложения')


class ApplicationStartCounter(BaseMetricCounter):
    class Meta:
        verbose_name = _('количество запусков приложения')
        verbose_name_plural = _('количество запусков приложения')


class TrainingTime(BaseMetricTime):
    training = models.ForeignKey('trainings.TrainingType', verbose_name=_('Вид тренировки'), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('время работы тренировки')
        verbose_name_plural = _('время работы тренировок')


class TrainingStartCounter(BaseMetricCounter):
    training = models.ForeignKey('trainings.TrainingType', verbose_name=_('Вид тренировки'), on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('количество запусков тренировки')
        verbose_name_plural = _('количество запусков тренировки')


