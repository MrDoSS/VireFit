from rest_framework.viewsets import ModelViewSet
from engine.metrics.models import ApplicationTime, ApplicationStartCounter, TrainingTime, TrainingStartCounter
from engine.metrics.serializers import ApplicationTimeSerializer, TrainingStartCounterSerializer, \
    TrainingTimeSerializer, ApplicationStartCounterSerializer


class ApplicationTimeViewSet(ModelViewSet):
    queryset = ApplicationTime.objects.all()
    serializer_class = ApplicationTimeSerializer


class ApplicationStartCounterViewSet(ModelViewSet):
    queryset = ApplicationStartCounter.objects.all()
    serializer_class = ApplicationStartCounterSerializer


class TrainingTimeViewSet(ModelViewSet):
    queryset = TrainingTime.objects.all()
    serializer_class = TrainingTimeSerializer


class TrainingStartCounterViewSet(ModelViewSet):
    queryset = TrainingStartCounter.objects.all()
    serializer_class = TrainingStartCounterSerializer
