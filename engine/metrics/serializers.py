from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from engine.metrics.models import ApplicationTime, TrainingTime, ApplicationStartCounter, TrainingStartCounter
from engine.models import Device

EXCLUDE = ('start_date',)


class ApplicationTimeSerializer(ModelSerializer):
    class Meta:
        model = ApplicationTime
        exclude = EXCLUDE


class ApplicationStartCounterSerializer(ModelSerializer):
    class Meta:
        model = ApplicationStartCounter
        exclude = EXCLUDE


class TrainingTimeSerializer(ModelSerializer):
    class Meta:
        model = TrainingTime
        exclude = EXCLUDE


class TrainingStartCounterSerializer(ModelSerializer):
    class Meta:
        model = TrainingStartCounter
        exclude = EXCLUDE
